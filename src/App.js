import PlayerList from "./components/PlayerList";
import {Container} from 'react-bootstrap'

function App() {
  return (
    <div className="App">
      <Container className="mb-5">
        <h1 className="text-center mt-5 mb-5">Football Players</h1>
        <PlayerList></PlayerList>
      </Container>
    </div>
  );
}

export default App;
