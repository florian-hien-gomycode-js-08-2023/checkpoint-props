import React from "react";
import {Card} from 'react-bootstrap'


function Player({ nom, equipe, image, nationalite, numeroMaillot, age }) {
  return (
    <div className="Player">
    <Card style={{width:'25em'}}>
      <Card.Img variant="top" src={image} style={{width:'100%', height:'398px'}}/>
      <Card.Body>
        <Card.Title>{nom}</Card.Title>
        <Card.Text>
          Team: {equipe} <br/>
          Nationality: {nationalite} <br/>
          Jersey Number: {numeroMaillot} <br/>
          Age: {age}
        </Card.Text>
      </Card.Body>
    </Card>
    </div>
  );
}

Player.defaultProps = {
  name: "Unknown",
  team: "Unknown",
  nationality: "Unknown",
  jerseyNumber: 0,
  age: 0,
  imageUrl: "https://via.placeholder.com/150",
};

export default Player;
