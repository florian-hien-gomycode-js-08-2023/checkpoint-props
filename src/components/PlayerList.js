import React from 'react';
import players from "./players";
import Player from "./Player";

function PlayerList(props) {
    return (
        
        <div className='PlayerList d-flex flex-wrap justify-content-between' style={{gap:'2em'}}>
            {players.map(el => <Player nom= {el.nom} image={el.image} equipe= {el.equipe} nationalite= {el.nationalite} numeroMaillot= {el.numeroMaillot} age= {el.age} />)}
        </div>
    );
}

export default PlayerList;
